<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 18-May-15
 * Time: 8:17 PM
 */
class deck {

    /**
     * array of card
     */
    private $cards = array();
    public function __construct(){}
    public function  initialiseDeck(){
        $suits = array(suit::club,suit::spade,suit::heart,suit::diamond);
        foreach($suits as $suit)
            for($i=2;$i<=14;$i++){
                $face = 0;
                switch($i) {
                    case 11:
                        $face = 'J';
                        break;
                    case 12:
                        $face = 'Q';
                        break;
                    case 13:
                        $face = 'K';
                        break;
                    case 14:
                        $face = 'A';
                        break;
                    default:
                        $face = $i;
                        break;
                }
                $this->cards[] = new card($face,$suit,true);
            }
    }

    /**
     * @param array $cards
     */
    public function setCards(array $cards){
        $this->cards = $cards;
    }

    /**
     * @return array
     */
    public function getCards(){
        return $this->cards;
    }

    public function getRandomCard(){
        $randIndex = rand(0,51);
        if($this->cards[$randIndex]->getInDeck())
            return $this->cards[$randIndex];
        return $this->getRandomCard();
    }
    public function removeFromDeck(card $cardToBeRemoved)
    {
        foreach($this->cards as $index => $card)
        {
            if($card == $cardToBeRemoved){
                return $card->setInDeck(false);
                break;
            }
        }
    }

}