<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 22-May-15
 * Time: 1:26 PM
 */

class helper {

    public static function loadContainer($controller,$view,$globalVariables=null){
        if($globalVariables!=null)
            extract($globalVariables);
        ob_start();
        include_once(root_dir."/".view_dir."/container.html.php");
        ob_flush();
    }
    public static function resolveURL($url){
        $url = str_replace("/".root_folder."/","",$url);

        $tempArray = explode("/",$url);
        $urlArray = array();
        foreach($tempArray as $urlStr){
            if(!isset($urlArray['controller']))
                $urlArray['controller'] = $urlStr;
            else if(!isset($urlArray['method']))
                $urlArray['method'] = $urlStr;
            else
                $urlArray[] = $urlStr;

        }
        return $urlArray;
    }
    public static function loadController($controller){
        @$controllerName = $controller."Controller";
        return new $controllerName;
    }
    public static function loadCSS(){
        $cssDir = scandir(root_dir."/".design_dir."/css/");
        foreach($cssDir as $fileName)
            if($fileName!="." and $fileName!="..")
                echo "<link rel=\"stylesheet\" href=\"http://".site_name."/".design_dir."/css/".$fileName."\" type=\"text/css\">";
    }
    public static function loadJS(){
        $jsDir = scandir(root_dir."/".design_dir."/js/");
        //first try and include jquery
        echo "<script type=\"text/javascript\" src=\"http://".site_name."/".design_dir."/js/jquery.min.js\"></script>";
        foreach($jsDir as $fileName)
            if($fileName!="." and $fileName!=".." and $fileName!="jquery.min.js")
                echo "<script type=\"text/javascript\" src=\"http://".site_name."/".design_dir."/js/".$fileName."\"></script>";
    }
    public static function getSuits(){
        $suits = new ReflectionClass("suit");
        return $suits->getConstants();
    }
    public static function getFaces(){
        $faces = array();
        for($i=2;$i<=14;$i++){
            $face = 0;
            switch($i) {
                case 11:
                    $face = 'J';
                    break;
                case 12:
                    $face = 'Q';
                    break;
                case 13:
                    $face = 'K';
                    break;
                case 14:
                    $face = 'A';
                    break;
                default:
                    $face = $i;
                    break;
            }
            $faces[] = $face;
        }
        return $faces;
    }
    public static function getHeaderTitle(){
        return "<h1>".site_title."</h1>";
    }
} 