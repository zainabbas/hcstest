<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 19-May-15
 * Time: 7:38 AM
 */

class BlackjackController extends controller{

    public function __construct(){

    }
    public function index($globalVariables=null) {
        if(!isset($globalVariables['template']))
            helper::loadContainer($this,root_dir."/".view_dir."/blackjack.html.php",$globalVariables);
        else
            helper::loadContainer($this,root_dir."/".view_dir."/".$globalVariables['template'],$globalVariables);
    }
    public function loadGame(){
        $blackjack = new blackjack();
    }
    public function loadTemplate($templatePath,$globalVariables=null){
        if($globalVariables!=null)
            extract($globalVariables);
        include_once($templatePath);
    }
    public function showScore($url,$request){
        $blackjack = new blackjack();
        $card1 = new card($request['face1'],$request['suit1'],true);
        $card2 = new card($request['face2'],$request['suit2'],true);
        $blackjack->setPlayerCards(array($card1,$card2));
        $this->index(array("template"=>"blackjack_show_score.html.php","score"=>$blackjack->calculateScore()));
    }
} 