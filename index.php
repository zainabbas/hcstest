<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 20-May-15
 * Time: 2:11 PM
 */


require_once("app/autoload.php");

config::declareConstants();

/*$suits = new ReflectionClass("suit");
//$deputyBackendDeveloper = new DeputyBackendDeveloper();
$default = new defaultController();

$deckObject = new deck();
$deckObject->initialiseDeck();
//var_dump($deputyBackendDeveloper);
$default->index();

var_dump($deckObject->removeFromDeck($deckObject->getRandomCard()));
var_dump($deckObject->getCards());
//echo $deckObject->getCards()[0]->getSuit();*/

//echo root_dir;
//echo "<br>";
//echo $_SERVER['REQUEST_URI'];

if($_SERVER['REQUEST_URI']!="/".root_folder."/") {
    $request = $_REQUEST;
    $url = helper::resolveURL($_SERVER['REQUEST_URI']);
    $controller = helper::loadController($url['controller']);
    if (isset($url['method']) and $url['method'] != "") {
        $functionName = $url['method'];
        $controller->$functionName($url,$request);
    }
    else
        $controller->index();
}
else{
    $default = new defaultController();
    $default->index();
}