<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 20-May-15
 * Time: 2:13 PM
 */

class TwitterController {
    public function loadTemplate($templatePath,$globalVariables=null){
        if($globalVariables!=null)
            extract($globalVariables);
        include_once($templatePath);
    }
    public function index($globalVariables=null) {
        if(!isset($globalVariables['template']))
            helper::loadContainer($this,root_dir."/".view_dir."/twitter.html.php",$globalVariables);
        else
            helper::loadContainer($this,root_dir."/".view_dir."/".$globalVariables['template'],$globalVariables);
    }

} 