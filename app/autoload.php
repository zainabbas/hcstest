<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 20-May-15
 * Time: 2:26 PM
 */

spl_autoload_register(null, false);

spl_autoload_extensions('.php, .class.php');

function configLoader($config) {
    $fileName = "app/config/".$config.'.php';
    if(!file_exists($fileName)){
        return false;
    }
    include_once($fileName);
}
function appClassLoader($class) {
    $fileName = "app/classes/".$class.'.php';
    if(!file_exists($fileName)){
        return false;
    }
    include_once($fileName);
}
function classLoader($class) {
    $fileName = "classes/".$class.'.php';
    if(!file_exists($fileName)){
        return false;
    }
    include_once($fileName);
}

function controllerLoader($controller) {
    $fileName = "controller/".$controller.'.php';
    if(!file_exists($fileName)){
        return false;
    }
    include_once($fileName);
}

spl_autoload_register('configLoader');
spl_autoload_register('appClassLoader');
spl_autoload_register('classLoader');
spl_autoload_register('controllerLoader');

?>