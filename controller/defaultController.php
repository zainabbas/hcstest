<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 20-May-15
 * Time: 6:29 PM
 */

class defaultController extends controller{
    public function loadTemplate($templatePath,$globalVariables=null){
        if($globalVariables!=null)
            extract($globalVariables);
        include_once($templatePath);
    }
    public function index($globalVariables=null) {
        if(!isset($globalVariables['template']))
            helper::loadContainer($this,root_dir."/".view_dir."/default.html.php",$globalVariables);
        else
            helper::loadContainer($this,root_dir."/".view_dir."/".$globalVariables['template'],$globalVariables);
    }
    //public function loadTemplate(array $includeFiles){
        //foreach($includeFiles as $fileName)
        //    $this->loadFile($fileName);
    //}
}