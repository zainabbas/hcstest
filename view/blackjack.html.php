<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 24-May-15
 * Time: 1:47 PM
 */
$suits = helper::getSuits();
$faces = helper::getFaces();
?>
<div class="">
    <div class="row">
        <div class="col-lg-12" style="text-align: center">
            <form method="POST" action="<?php echo "http://".site_name."/blackjack/showscore/"; ?>">
                <h4>Please Select your cards</h4>
                Card 1 :
                <select name="suit1" id="suit1">
                    <?php
                    foreach($suits as $suitName => $suitValue){?>
                        <option value="<?php echo $suitValue; ?>"><?php echo $suitName; ?></option>
                    <?php
                    }?>
                </select>
                <select name="face1" id="face1">
                    <?php
                    foreach($faces as $faceName){?>
                        <option value="<?php echo $faceName; ?>"><?php echo $faceName; ?></option>
                    <?php
                    }?>
                </select>
                <br/>
                Card 2 :
                <select name="suit2" id="suit2">
                    <?php
                    foreach($suits as $suitName => $suitValue){?>
                        <option value="<?php echo $suitValue; ?>"><?php echo $suitName; ?></option>
                    <?php
                    }?>
                </select>
                <select name="face2" id="face2">
                    <?php
                    foreach($faces as $faceName){?>
                        <option value="<?php echo $faceName; ?>"><?php echo $faceName; ?></option>
                    <?php
                    }?>
                </select>
                <br/>
                <button type="submit">Calculate Score</button>
            </form>
        </div>
    </div>
</div>