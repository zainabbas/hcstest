<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 20-May-15
 * Time: 6:32 PM
 */

//$rootDirectoryName = 'site';
class config
{
    public static function declareConstants()
    {
        DEFINE('root_folder', "hcstest");
        DEFINE('site_name', $_SERVER['SERVER_NAME']."/".root_folder);
        DEFINE('root_dir', $_SERVER['DOCUMENT_ROOT'] . root_folder);
        DEFINE('default_template', "default.html.php");
        DEFINE('view_dir', "view");
        DEFINE('design_dir', "design");
        DEFINE('images_dir', "images");
        //DEFINE('site_title', "Home Code Screening Test");
        DEFINE('site_title', "HCS Test");
    }
}