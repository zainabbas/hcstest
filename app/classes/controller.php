<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 25-May-15
 * Time: 8:08 AM
 */

abstract class controller {

    public abstract function index($globalVariables=null);
    public abstract function loadTemplate($template,$globalVariables=null);
}