<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 18-May-15
 * Time: 8:16 PM
 */
class card {

    private $value;
    private $face;
    private $suit;
    private $inDeck;

    public function __construct($face,$suit,$inDeck=true){
        $this->face = $face;
        $this->suit = $suit;
        $this->inDeck = $inDeck;
        $this->value = $this->calculateValue($this->face);
    }

    public function calculateValue($face){
        if(is_numeric($face)){
            return $face;
        }
        switch($face){
            case 'A':
                return 11;
                break;
            case 'J':
                return 10;
                break;
            case 'Q':
                return 10;
                break;
            case 'K':
                return 10;
                break;
            default:
                break;
        }
    }

    /**
     * @param $suit
     * @return $this
     */
    public function setType($suit){
        $this->suit = $suit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSuit(){
        return $this->suit;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value){
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue(){
        return $this->value;
    }

    /**
     * @param $face
     * @return $this
     */
    public function setFace($face){
        $this->face = $face;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFace(){
        return $this->face;
    }

    /**
     * @return bool
     */
    public function getInDeck(){
        return $this->inDeck;
    }

    /**
     * @param $inDeck
     * @return $this
     */
    public function setInDeck($inDeck){
        $this->inDeck = $inDeck;
        return $this;
    }
} 