<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 19-May-15
 * Time: 7:45 AM
 */

class blackjack {
    /**
     * @var deck
     */
    public $deckOfCards;
    /**
     * @var array
     */
    public $playerCards;

    /**
     *
     */
    public function __construct(){
        $this->deckOfCards = new deck();
        $this->deckOfCards->initialiseDeck();
    }

    /**
     *
     */
    public function initialiseDeck(){}

    /**
     *
     */
    public function initialisePlayerInput(){}

    /**
     *
     */
    public function startGame(){}

    /**
     *
     */
    public function calculateScore(){
        $score = 0;
        foreach ($this->playerCards as $card) {
            $score += $card->getValue();
        }
        return $score;
    }

    /**
     * @param $playerCards
     * @return $this
     */
    public function setPlayerCards(array $playerCards){
        $this->playerCards = $playerCards;
        return $this;
    }
    public function getPlayerCards(){
        return $this->playerCards;
    }
} 