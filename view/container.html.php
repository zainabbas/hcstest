<?php
?>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <?php
        //include css
        helper::loadCSS();
        ?>
        <title></title>
    </head>
    <body>
    <div class="row">
        <div class="col-lg-12" style="text-align: center">
            <a href="<?php echo "http://".site_name; ?>" class="home_link"><?php echo helper::getHeaderTitle(); ?></a>
        </div>
        <div class="row">
            <?php
                $controller->loadTemplate($view,$globalVariables);
            ?>
        </div>
    </div>
    </body>
    <?php
    //include javascript
    helper::loadJS();
    ?>
</html>